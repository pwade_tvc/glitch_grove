﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    [SerializeField] AudioClip audioClip = default;
    AudioSource audioSource;
    
    void Start() {
        audioSource = GetComponent<AudioSource>();
        SetVolume(PlayerPrefsWrapper.GetVolume());
    }

    public void SetVolume(float volume) {
        audioSource.volume = volume;
    }

    public void SetAudioClip(AudioClip audioClipToSet) {
        audioClip = audioClipToSet;
    }

    public void StartMusic() {
        audioSource.Play();
    }

    public void StopMusic() {
        audioSource.Stop();
    }
}
