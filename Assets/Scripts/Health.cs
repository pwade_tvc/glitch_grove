﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    [SerializeField] float health = 100f;
    [SerializeField] GameObject deathVFXPrefab = default;

    public void TakeDamage(float damage) {
        health -= damage;
        if (health <= 0) {
            HandleDeathEvent();
            Destroy(gameObject);
        }
    }

    private void HandleDeathEvent() {
        TriggerDeathVFX();
        var attacker = GetComponentInParent<Attacker>();
        if (attacker) {
            FindObjectOfType<BankAccount>().AddToBalance(attacker.GetReward());
        }
    }

    private void TriggerDeathVFX() {
        if (deathVFXPrefab) {
            var deathVFX = Instantiate(deathVFXPrefab, transform.position, transform.rotation);
            Destroy(deathVFX, 1f);
        }
    }
}
