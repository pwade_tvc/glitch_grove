﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class DefenderButton : MonoBehaviour {

    [SerializeField] Defender defenderPrefab = default;
    //[SerializeField] TextMeshProUGUI costText = default;

    [Header("DEBUG")]
    [SerializeField] bool buttonActivated = false;

    public bool ButtonActivated { get => buttonActivated; }

    private void Start() {
        SetCostText();
    }

    private void SetCostText() {
        var costText = GetComponentInChildren<TextMeshProUGUI>();
        if (costText) {
            costText.text = defenderPrefab.GetComponent<Defender>().Cost.ToString();
        } else {
            Debug.LogWarning("Button does not have a TextMeshProUGUI component in any child.");
        }
    }

    public void OnMouseDown() {
        ResetButtons();
        ActivateButton();
        FindObjectOfType<DefenderSpawner>().SetDefenderPrefab(defenderPrefab);
    }

    private void ResetButtons() {
        var buttons = FindObjectsOfType<DefenderButton>();
        foreach (DefenderButton button in buttons) {
            button.DeactivateButton();
        }
    }

    private void ActivateButton() {
        GetComponent<SpriteRenderer>().color = Color.white;
        buttonActivated = true;
    }

    private void DeactivateButton() {
        GetComponent<SpriteRenderer>().color = Color.grey;
        buttonActivated = false;
    }

}
