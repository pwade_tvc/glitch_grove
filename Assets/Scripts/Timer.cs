﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {


    [SerializeField] float levelTimeInSeconds = 10;

    bool levelFinished = false;

    void Update() {
        if (!levelFinished) {
            GetComponent<Slider>().value = Time.timeSinceLevelLoad / levelTimeInSeconds;
            bool timeUp = Time.timeSinceLevelLoad >= levelTimeInSeconds;
            if (timeUp) {
                GetComponentInChildren<Animator>().SetBool("timerFinished", true);
                FindObjectOfType<LevelController>().TimerFinished();
                levelFinished = true;
            }
        }
    }
}
