﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

    [SerializeField] float loadDelay = 0f;

    int currentSceneIndex = 0;

    // Start is called before the first frame update
    void Start() {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (currentSceneIndex == 0) {
            StartCoroutine(LoadNextSceneAfterDelay());
        }
    }

    private IEnumerator LoadNextSceneAfterDelay() {
        yield return new WaitForSeconds(loadDelay);
        LoadNextScene();
    }

    public void LoadStartingScene() {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    public void LoadNextScene() {
        Time.timeScale = 1;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    public void RestartScene() {
        Time.timeScale = 1;
        SceneManager.LoadScene(currentSceneIndex);
    }

    public void LoadGameOverScene() {
        Time.timeScale = 1;
        SceneManager.LoadScene("Game Over Scene");
    }

    public void LoadOptionsScene() {
        SceneManager.LoadScene("Options Screen");
    }

    public void QuitGame() {
        Application.Quit();
    }

}
