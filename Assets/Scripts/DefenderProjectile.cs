﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderProjectile : MonoBehaviour {

    [SerializeField] GameObject projectilePrefab = default;
    [SerializeField] GameObject gun = default;
    
    AttackerSpawner laneSpawner = default;
    Animator animator = default;
    GameObject projectileParent = default;
    const string PROJECTILE_PARENT_NAME = "Projectiles";

    private void Start() {
        laneSpawner = GetLaneSpawner();
        animator = GetComponent<Animator>();
        CreateProjectileParent();
    }

    private void CreateProjectileParent() {
        projectileParent = GameObject.Find(PROJECTILE_PARENT_NAME);
        if (!projectileParent) {
            projectileParent = new GameObject(PROJECTILE_PARENT_NAME);
        }
    }

    private void Update() {
        animator.SetBool("isAttacking", IsAttackerInLane());
    }

    private AttackerSpawner GetLaneSpawner() {
        var attackerSpawners = FindObjectsOfType<AttackerSpawner>();
        foreach (AttackerSpawner attackerSpawner in attackerSpawners) {
            var spawnerY = attackerSpawner.transform.position.y;
            var defenderY = transform.position.y;

            // This is from the lesson and DOES NOT WORK
            //bool inLane = (spawnerY - defenderY <= Mathf.Epsilon);
            // You could add Mathf.Abs to fix it:
            //bool inLane = (Mathf.Abs(spawnerY - defenderY) <= Mathf.Epsilon);
            // In my opinion, since we already know the spawners are on exact grid points
            // and the attackers spawn on exact grid points, we should just be able to compare the grid points.
            // This can be strenghtend by makin+g the lane assignment a property of the AttackerSpawner
            // and then dynamically spawning the AttackerSpawners via their Start() method.  Alternatively,
            // if this ends up breaking due to imprecision, casting to int or using Mathf.Round() would seem 
            // more clear to me than the above.
            if (spawnerY == defenderY) {
                return attackerSpawner;
            }
            
        }
        return null;
    }

    private bool IsAttackerInLane() {
        return laneSpawner.transform.childCount > 0;
    }

    public void Fire() {
        var projectile = Instantiate<GameObject>(projectilePrefab, gun.transform.position, transform.rotation);
        projectile.transform.parent = projectileParent.transform;
    }

}
