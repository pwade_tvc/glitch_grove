﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defender : MonoBehaviour {

    [SerializeField] int cost = 0;

    public int Cost { get => cost; }

    public void AddToBankAccountBalance(int amount) {
        FindObjectOfType<BankAccount>().AddToBalance(amount);
    }

}
