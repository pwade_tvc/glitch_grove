﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BankAccount : MonoBehaviour
{
    [SerializeField] int balance = 100;
    TextMeshProUGUI bankAccountText = default;

    public int Balance { get => balance; }

    // Start is called before the first frame update
    void Start() {
        bankAccountText = GetComponent<TextMeshProUGUI>();
        UpdateBankAccountText();
    }

    private void UpdateBankAccountText() {
        bankAccountText.text = balance.ToString();
    }

    public void AddToBalance(int amountToAdd) {
        balance += amountToAdd;
        UpdateBankAccountText();
    }

    public void DeductFromBalance(int amountToDeduct) {
        if (amountToDeduct <= balance) {
            balance -= amountToDeduct;
            UpdateBankAccountText();
        }
        
    }
}
