﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelController : MonoBehaviour
{
    [Header("Config")]
    [SerializeField] GameObject winCanvas = default;
    [SerializeField] AudioClip winSound = default;
    [SerializeField] GameObject loseCanvas = default;
    [SerializeField] AudioClip loseSound = default;
    [SerializeField] float levelChangeDelay = 4f;

    [Header("DEBUG")]
    [SerializeField] int attackerCount = 0;
    [SerializeField] bool timerFinished = false;
    [SerializeField] bool gameLost = false;


    private void Start() {
        winCanvas.SetActive(false);
        loseCanvas.SetActive(false);
    }

    public void AttackerSpawned() {
        attackerCount++;
    }

    public void AttackerKilled() {
        attackerCount--;
        if (attackerCount == 0 && timerFinished && !gameLost) {
            StartCoroutine(HandleWinEvent());
        }
    }

    public void TimerFinished() {
        timerFinished = true;
        StopSpawners();
    }

    private IEnumerator HandleWinEvent() {
        winCanvas.GetComponentInChildren<TextMeshProUGUI>().text = "Level Complete!";
        winCanvas.SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(winSound);
        yield return new WaitForSeconds(levelChangeDelay);
        FindObjectOfType<SceneController>().LoadNextScene();
    }

    public void LevelLost(float delay) {
        gameLost = true;
        loseCanvas.SetActive(true);
        StopSpawners();
        GetComponent<AudioSource>().PlayOneShot(loseSound);
        StartCoroutine(StopGame(delay));
    }

    public IEnumerator StopGame(float delay) {
        yield return new WaitForSeconds(delay);
        Time.timeScale = 0;
    }

    private void StopSpawners() {
        var spawners = FindObjectsOfType<AttackerSpawner>();
        foreach (AttackerSpawner spawner in spawners) {
            spawner.StopSpawning();
        }
    }
}
