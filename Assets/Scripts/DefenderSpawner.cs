﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour {

    Defender defenderPrefab = default;
    GameObject defenderParent = default;
    const string DEFENDER_PARENT_NAME = "Defenders";

    private void Start() {
        CreateDefenderParent();
    }

    private void CreateDefenderParent() {
        defenderParent = GameObject.Find(DEFENDER_PARENT_NAME);
        if (!defenderParent) {
            defenderParent = new GameObject(DEFENDER_PARENT_NAME);
        }
    }

    private void OnMouseDown() {
        AttemptToPlaceDefenderAt(GetGridLocation(Input.mousePosition));
    }

    public void SetDefenderPrefab(Defender selectedDefender) {
        defenderPrefab = selectedDefender;
    }

    private void AttemptToPlaceDefenderAt(Vector2 gridLocation) {
        if (HasLaneSpawner(gridLocation)) { 
            var bankAccount = FindObjectOfType<BankAccount>();
            int defenderCost = defenderPrefab.Cost;
            if (bankAccount.Balance >= defenderCost) {
                SpawnDefender(gridLocation);
                bankAccount.DeductFromBalance(defenderCost);
            }
        }
    }
    

    private bool HasLaneSpawner(Vector2 gridLocation) {
        bool hasLaneSpawner = false;
        var attackerSpawners = FindObjectsOfType<AttackerSpawner>();
        foreach (AttackerSpawner attackerSpawner in attackerSpawners) {
            var spawnerY = attackerSpawner.transform.position.y;
            if (spawnerY == gridLocation.y) {
                hasLaneSpawner = true;
            }
        }
        return hasLaneSpawner;
    }

    private Vector2 GetGridLocation(Vector2 mousePosition) {
        var worldPos = Camera.main.ScreenToWorldPoint(mousePosition);
        var gridLocation = SnapToGrid(worldPos);
        //Debug.Log(mousePosition + ", " + worldPos + ", " + gridLocation);
        return gridLocation;
    }

    private Vector2 SnapToGrid(Vector2 worldPos) {
        return new Vector2(Mathf.Round(worldPos.x), Mathf.Round(worldPos.y));
    }

    private void SpawnDefender(Vector2 gridLocation) {
        gridLocation = new Vector2(Mathf.Round(gridLocation.x), Mathf.Round(gridLocation.y));
        var newDefender = Instantiate<Defender>(defenderPrefab, gridLocation, Quaternion.identity);
        newDefender.transform.parent = defenderParent.transform;
    }
}
