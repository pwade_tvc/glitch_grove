﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsWrapper : MonoBehaviour {

    const string VOLUME_KEY = "Volume";
    const float DEFAULT_VOLUME = 0.5f;
    const float MIN_VOLUME = 0f;
    const float MAX_VOLUME = 1f;

    const string DIFFICULTY_KEY = "Difficulty";
    const int DEFAULT_DIFFICULTY = 2;
    const int MIN_DIFFICULTY = 1;
    const int MAX_DIFFICULTY = 10;

    public static bool HasVolumeSetting() {
        return PlayerPrefs.HasKey(VOLUME_KEY);
    }

    public static void SetVolume(float volume) {
        if (volume >= MIN_VOLUME && volume <= MAX_VOLUME) {
            PlayerPrefs.SetFloat(VOLUME_KEY, volume);
        } else {
            Debug.LogWarning("Unable to save volume setting.  Value out of range.");
        }
    }

    public static float GetVolume() {
        return PlayerPrefs.GetFloat(VOLUME_KEY);
    }

    public static float GetDefaultVolume() {
        return DEFAULT_VOLUME;
    }

    public static bool HasDifficultySetting() {
        return PlayerPrefs.HasKey(DIFFICULTY_KEY);
    }

    public static void SetDifficulty(int difficulty) {
        if (difficulty >= MIN_DIFFICULTY && difficulty <= MAX_DIFFICULTY) {
            PlayerPrefs.SetInt(DIFFICULTY_KEY, difficulty);
        } else {
            Debug.LogWarning("Unable to save difficulty setting.  Value out of range.");
        }
    }

    public static int GetDifficulty() {
        return PlayerPrefs.GetInt(DIFFICULTY_KEY);
    }

    public static int GetDefaultDifficulty() {
        return DEFAULT_DIFFICULTY;
    }

}
