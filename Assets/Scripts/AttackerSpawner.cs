﻿using System.Collections;
using UnityEngine;

public class AttackerSpawner : MonoBehaviour {

    [Header("Config")]
    [SerializeField] Attacker[] attackerPrefabArray = default;
    [SerializeField] float minSpawnDelay = 1f;
    [SerializeField] float maxSpawnDelay = 5f;

    [Header("DEBUG")]
    [SerializeField] bool spawn = true;

    // Start is called before the first frame update
    IEnumerator Start() {
        while (spawn) {
            yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));

            // It is possible that the attackers have won the game during the delay so
            // we check the spawn flag again just before spawning to be sure.  This is 
            // mostly an aesthetic choice.  It looks weird to have attackers continue
            // to spawn after the base has blown up.
            if (spawn) {
                SpawnAttacker();
            }
        }
        
    }

    private void SpawnAttacker() {
        Attacker attackerToSpawn = GetRandomAttacker();
        var spawnedAttacker = Instantiate<Attacker>(attackerToSpawn, transform.position, transform.rotation);
        spawnedAttacker.transform.parent = transform;
    }

    private Attacker GetRandomAttacker() {
        var attackerIndex = Random.Range(0, attackerPrefabArray.Length);
        return attackerPrefabArray[attackerIndex];
    }

    public void StopSpawning() {
        spawn = false;
    }
}
