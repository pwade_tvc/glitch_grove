﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Base : MonoBehaviour
{
    [Header("Config")]
    [SerializeField] GameObject deathVFXPrefab = default;
    [SerializeField] float gameOverDelay = 4f; // TODO: Calculate this from numberOfVFXToTrigger * delayBetweenVFX?

    [Header("Settings for VFX on death")]
    [SerializeField] private int numberOfVFXToTrigger = 20;
    [SerializeField] private float delayBetweenVFX = 0.1f;
    [SerializeField] private float vfxYmin = 0.0f;
    [SerializeField] private float vfxYmax = 7.0f;
    [SerializeField] private float vfxXmin = -0.5f;
    [SerializeField] private float vfxXmax = 0.0f;

    bool baseDestroyed = false;

    private void OnTriggerEnter2D(Collider2D otherCollider) {
        var attacker = otherCollider.GetComponent<Attacker>();
        if (attacker) {
            var lives = FindObjectOfType<Lives>();
            lives.ReduceLives(attacker.InfiltrationDamage);
            if (lives.LivesLeft <= 0 && !baseDestroyed) {
                baseDestroyed = true;
                StartCoroutine(GameOver());
            }
            Destroy(otherCollider.gameObject);
        }
    }

    private IEnumerator GameOver() {
        FindObjectOfType<LevelController>().LevelLost(gameOverDelay);
        StartCoroutine(TriggerDeathVFX());
        yield return new WaitForSeconds(gameOverDelay);
    }
    
    private IEnumerator TriggerDeathVFX() {
        if (deathVFXPrefab) {
            var offset = new Vector2(transform.position.x, transform.position.y);
            for (int i = 0; i < numberOfVFXToTrigger; i++) {
                offset.x = Random.Range(vfxXmin, vfxXmax);
                offset.y = Random.Range(vfxYmin, vfxYmax);
                var deathVFX = Instantiate(deathVFXPrefab, offset, transform.rotation);
                Destroy(deathVFX, 1f);
                yield return new WaitForSeconds(delayBetweenVFX);
            }
        }
    }

}
