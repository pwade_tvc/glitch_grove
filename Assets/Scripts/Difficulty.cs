﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Difficulty : MonoBehaviour
{

    private List<DifficultySetting> difficultySettings = new List<DifficultySetting>();
    private DifficultySetting currentDifficultySettings;



    private void Awake() {
        // Layout                                     Name              Lives  Speed* Health* Reward*
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 1",    5,  1.0f,   0.0f,   1.5f));
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 2",    5,  1.1f,   0.0f,   1.25f));
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 3",    4,  1.1f,   1.2f,   1.0f));
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 4",    3,  1.2f,   1.2f,   1.5f));
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 5",    3,  1.4f,   1.2f,   1.5f));
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 6",    2,  1.4f,   1.5f,   1.5f));
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 7",    1,  1.5f,   1.5f,   1.5f));
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 8",    1,  1.75f,  1.75f,  1.5f));
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 9",    1,  1.75f,  2.0f,   1.5f));
        difficultySettings.Add(new DifficultySetting("Difficulty Lvl 10",   1,  2.0f,   3.0f,   1.5f));
        UpdateDifficultySettings();
        Debug.Log("Starting difficulty setting: " + GetName());
    }

    public void UpdateDifficultySettings() {
        currentDifficultySettings = difficultySettings[PlayerPrefsWrapper.GetDifficulty() - 1];
        Debug.Log("New difficulty setting: " + GetName());
    }

    public string GetName() {
        return currentDifficultySettings.Name;
    }

    public int GetLives() {
        return currentDifficultySettings.Lives;
    }

    public float GetAttackerSpeedModifier() {
        return currentDifficultySettings.AttackerSpeedModifier;
    }

    public float GetAttackerHealthModifier() {
        return currentDifficultySettings.AttackerHealthModifier;
    }

    public float GetAttackerRewardModifier() {
        return currentDifficultySettings.AttackerRewardModifier;
    }

    private class DifficultySetting {
        private string _name;
        private int _lives;
        private float _attackerSpeedModifier;
        private float _attackerHealthModifier;
        private float _attackerRewardModifier;

        public DifficultySetting(string name, int lives, float attackerSpeedModifier, float attackerHealthModifier, float attackerRewardModifier) {
            _name = name;
            _lives = lives;
            _attackerSpeedModifier = attackerSpeedModifier;
            _attackerHealthModifier = attackerHealthModifier;
            _attackerRewardModifier = attackerRewardModifier;
        }

        public string Name { get => _name; set => _name = value; }
        public int Lives { get => _lives; set => _lives = value; }
        public float AttackerSpeedModifier { get => _attackerSpeedModifier; set => _attackerSpeedModifier = value; }
        public float AttackerHealthModifier { get => _attackerHealthModifier; set => _attackerHealthModifier = value; }
        public float AttackerRewardModifier { get => _attackerRewardModifier; set => _attackerRewardModifier = value; }
    }


}
