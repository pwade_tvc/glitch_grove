﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : MonoBehaviour {

    [SerializeField] float damage = 15f;

    private void OnTriggerEnter2D(Collider2D otherCollider) {
        GameObject potentialDefender = otherCollider.gameObject;
        Defender defender = potentialDefender.GetComponent<Defender>();
        if (defender) {
            if (defender.GetComponent<Tombstone>()) {
                JumpOverDefender(defender);
            } else {
                transform.position = new Vector2(potentialDefender.transform.position.x + 1, transform.position.y);
                GetComponent<Attacker>().Attack(potentialDefender, damage);
            }
        }
    }

    private void JumpOverDefender(Defender defenderToJumpOver) {
        GetComponent<Animator>().SetTrigger("jumpTrigger");
    }

}
