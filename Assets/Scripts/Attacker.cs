﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour {

    [Header("Config")]
    [SerializeField] [Range(0f,10f)] float baseMoveSpeed = 1f;
    [SerializeField] int infiltrationDamage = 1;
    [SerializeField] float reward = 1f;

    [Header("DEBUG")]
    [SerializeField] float currentMoveSpeed = 0f;
    [SerializeField] bool moveEnabled = false;

    GameObject currentTarget;
    float currentDamage;
    Animator animator;
    Difficulty difficultySettings;

    public int InfiltrationDamage { get => infiltrationDamage; }

    private void Awake() {
        FindObjectOfType<LevelController>().AttackerSpawned();
    }

    private void Start() {
        currentMoveSpeed = baseMoveSpeed;
        animator = GetComponent<Animator>();
        difficultySettings = FindObjectOfType<Difficulty>();
    }

    private void Update() {
        if (moveEnabled) {
            transform.Translate(Vector2.left * Time.deltaTime * currentMoveSpeed);
        }
    }

    private void OnDestroy() {
        var levelController = FindObjectOfType<LevelController>();
        if (levelController) {
            levelController.AttackerKilled();
        }
    }

    public int GetReward() {
        return Mathf.CeilToInt(reward * difficultySettings.GetAttackerRewardModifier());
    }

    public void Attack(GameObject target, float damage) {
        currentTarget = target;
        currentDamage = damage;
        animator.SetBool("isAttacking", true);
    }

    public void StrikeCurrentTarget() {
        if (currentTarget) {
            var health = currentTarget.GetComponent<Health>();
            if (health) {
                health.TakeDamage(currentDamage);
            }
        } else {
            animator.SetBool("isAttacking", false);
        }
    }

    public void StartMoving() {
        moveEnabled = true;
    }

    public void StopMoving() {
        moveEnabled = false;
    }

    public void ResetMoveSpeed() {
        currentMoveSpeed = baseMoveSpeed;
    }

    public void SetMovementSpeed(float speed) {
        currentMoveSpeed = speed;
    }
}
