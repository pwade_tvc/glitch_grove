﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour {


    [SerializeField] Slider volumeSlider = default;
    [SerializeField] Slider difficultySlider = default;

    Difficulty difficultySettings;

    void Start() {
        SetVolumeFromPrefs();
        volumeSlider.onValueChanged.AddListener(delegate { VolumeChanged(); });

        SetDifficultyFromPrefs();
        difficultySlider.onValueChanged.AddListener(delegate { DifficultyChanged(); });
        UpdateDifficultySettings();        
    }

    private void UpdateDifficultySettings() {
        if (!difficultySettings) {
            difficultySettings = FindObjectOfType<Difficulty>();
        }
        difficultySettings.UpdateDifficultySettings();
    }

    private void SetVolumeFromPrefs() {
        if (PlayerPrefsWrapper.HasVolumeSetting()) {
            volumeSlider.value = PlayerPrefsWrapper.GetVolume();
        } else {
            volumeSlider.value = PlayerPrefsWrapper.GetDefaultVolume();
        }
    }

    private void VolumeChanged() {
        var musicPlayer = FindObjectOfType<MusicPlayer>();
        if (musicPlayer) {
            musicPlayer.SetVolume(volumeSlider.value);
        } else {
            Debug.LogWarning("No music player found.");
        }
    }

    private void SetDifficultyFromPrefs() {
        if (PlayerPrefsWrapper.HasDifficultySetting()) {
            difficultySlider.value = PlayerPrefsWrapper.GetDifficulty();
        } else {
            difficultySlider.value = PlayerPrefsWrapper.GetDefaultDifficulty();
        }
    }

    private void DifficultyChanged() {
        Debug.Log("Difficulty changed to " + difficultySlider.value);
    }

    void Update() {
        // Nothing to do here.  Differs from lesson 169 where updates to objects
        // are done in Update() vs. using OnChange events like I've done.
    }

    public void SaveAndExit() {
        PlayerPrefsWrapper.SetVolume(volumeSlider.value);
        PlayerPrefsWrapper.SetDifficulty((int)difficultySlider.value);
        
        FindObjectOfType<SceneController>().LoadStartingScene();
    }

    public void SetDefaults() {
        volumeSlider.value = PlayerPrefsWrapper.GetDefaultVolume();
        difficultySlider.value = PlayerPrefsWrapper.GetDefaultDifficulty();
        difficultySettings.UpdateDifficultySettings();
    }
}
