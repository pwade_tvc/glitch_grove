﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Lives : MonoBehaviour
{
    [SerializeField] int livesLeft = 3;

    TextMeshProUGUI livesRemainingText = default;

    public int LivesLeft { get => livesLeft; }

    void Start() {
        livesRemainingText = GetComponent<TextMeshProUGUI>();
        SetLivesBasedOnDifficultyLevel();
        UpdateLivesRemainingText();
    }

    private void SetLivesBasedOnDifficultyLevel() {
        livesLeft = FindObjectOfType<Difficulty>().GetLives();
    }

    private void UpdateLivesRemainingText() {
        livesRemainingText.text = livesLeft.ToString();
    }

    public void AddLives(int amountToAdd) {
        livesLeft += amountToAdd;
        UpdateLivesRemainingText();
    }

    public void ReduceLives(int livesLost) {
        livesLeft -= livesLost;
        if (livesLeft < 0) {
            livesLeft = 0;
        }
        UpdateLivesRemainingText();
    }
}
