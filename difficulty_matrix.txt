// Lvl  Desc.       Lives   Speed Mod   Health Mod    Reward Mod
// 1    Easy        5       1.0         0.0			  1.5			
// 2    Easy2       5       1.1         0.0   		  1.25
// 3    Easy3       4       1.1         1.2			  1.0
// 4    Moderate    3       1.2         1.2			  1.0
// 5    Moderate2   3       1.4         1.2			  1.0
// 6    Moderate3   2       1.4         1.5			  1.0
// 7    Hard        1       1.5         1.5 		  0.9
// 8    Hard2       1       1.75        1.75		  0.8
// 9    Hard3       1       1.75        2.0			  0.6
// 10   Suicide     1       2.0         3.0			  0.5